package main

import (
	"text/template"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// Page sirve para la estructura de páginas
type Page struct {
	Title string
	Body  []byte
}

func (p *Page) save() error {
	filename := fmt.Sprintf("./data/%s.txt", p.Title)
	err := ioutil.WriteFile(filename, p.Body, 0600)
	return err
}

func loadPage(title string) (*Page, error) {
	filename := fmt.Sprintf("./data/%s.txt", title)
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	page := &Page{Title: title, Body: body}
	return page, err
}

func showHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/show/"):]
	p, err := loadPage(title)
	if err != nil {
		fmt.Fprintf(w, "<h2 style='color:red;'>%s</h2>", err)
	}
	
	// template files
	t, err := template.ParseFiles("./views/show.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	t.Execute(w, p)
}

func editHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/edit/"):]
	page, err := loadPage(title)
	if err != nil {
		page = &Page{Title: title}
	}
	t, err := template.ParseFiles("./views/edit.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	t.Execute(w, page)
}

func welcomeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Bienvenido</h1>")
}

func saveHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/save/"):]
	body := r.FormValue("body")
	page := &Page{Title: title, Body: []byte(body)}
	page.save()
	http.Redirect(w, r, "/show/"+title,http.StatusFound)
}

func main() {
	fs := http.FileServer(http.Dir("./public"))
	http.Handle("/css/", fs)
	http.HandleFunc("/show/", showHandler)
	http.HandleFunc("/edit/", editHandler)
	http.HandleFunc("/save/", saveHandler)
	http.HandleFunc("/", welcomeHandler)
	log.Println("Listening on http://localhost:3030")
	http.ListenAndServe(":3030", nil)
}
